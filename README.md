# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do FPGA Lattice LCMXO2-1200HC-4SG32 (https://www.latticesemi.com/Products/FPGAandCPLD/MachXO2).

- FPGA z serii MachXO2:
    - 1280 LUTs
    - 10 Kbits distributed RAM
    - 64 Kbits EBR SRAM
    - 7x EBR SRAM Blocks (9 Kbits each)
    - 64 Kbits User Flash Memory
    - Hardened functions:
        - 1x PLL
        - 2x I2C
        - 1x SPI
        - 1x timer/counter
- Zasilanie:
    - 5V z gniazda USB 2.0 Micro B
    - 5V lub 3.3V z pinów
- Opcjonalny oscylator kwarcowy 16MHz
- LED *User*
- Pin headers 2x16, raster 2.54mm
- Złącze JTAG 2x5, raster 1.27mm

# Projekt PCB

Schemat: [doc/FPGA_EVB_M01_V1_0_SCH.pdf](doc/FPGA_EVB_M01_V1_0_SCH.pdf)

Widok 3D: [doc/FPGA_EVB_M01_V1_0_3D.pdf](doc/FPGA_EVB_M01_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
